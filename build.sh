#!/bin/bash

# Jump to "self" folder
cd "$(dirname "$0")"

# Containers build
docker compose --env-file .env -f ./docker-compose.yml build --force-rm