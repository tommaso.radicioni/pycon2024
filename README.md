# RAGtime - A Simple Pipeline for Chat with Your Docs

## Index

- [Introduction](#introduction)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Running the Application](#running-the-application)
- [Technical Overview](#technical-overview)
  - [Document Storage and Retrieval with Qdrant](#document-storage-and-retrieval-with-qdrant)
  - [Building the RAG Pipeline with Haystack](#building-the-rag-pipeline-with-haystack)
- [Codebase Explained](#codebase-explained)
  - [qdrant_document_store.py](#qdrant_document_storcpy)
  - [ragtime_app.py](#ragtime_apppy)
  - [rag_retriever.py](#rag_retrieverpy)
  - [constants.py](#constantspy)
- [Docker Setup](#docker-setup)
- [Contact](#contact)


## Getting Started

Welcome to RAGtime, a minimal implementation of a Retrieval Augmented Generation (RAG) Pipeline designed to facilitate chatting with your documents. This project was presented at Pycon 2024 and aims to provide an intuitive user interface for interacting with documents through a chat interface.

These instructions will guide you through setting up and running the RAGtime project on your local machine for development and testing purposes.

### Prerequisites

Before you begin, ensure you have the following installed on your machine:

- Docker
- Docker Compose

If you don't have Docker installed, you can download it from [Docker's official website](https://www.docker.com/products/docker-desktop).

### Installation

1. Clone the repository to your local machine:

```bash
git clone https://gitlab.com/tommaso.radicioni/pycon2024.git
cd pycon2024
```

2. Add an `.env` file with your `"HUGGINGFACE_API_KEY"`, `"GROQ_API_KEY"` and `"OPENAI_API_KEY"`. To get your Hugging Face API key, follow these instructions:

If you haven't already, sign up for a Hugging Face account by visiting [Hugging Face's website](https://huggingface.co/join). Fill out the registration form and complete the verification process. In your account dashboard, look for the "API Tokens" section. Here, you'll see an option to create a new token. Click on "New token".

Give your new token a descriptive name so you can remember its purpose later. Now that you have your API key, you can use it in this application to authenticate requests to Hugging Face services. Replace `"HUGGINGFACE_API_KEY"` with the actual API key you copied earlier.

To obtain a Groq OpenAI API key. Go to https://groq.ai/ and sign up for a Groq account if you don't already have one. Then, log in to your Groq account and navigate to the API keys section in your account settings. Finally, generate a new API key and copy the generated API key. To get your OpenAI API key, go to https://openai.com/, sign up for an account if you don't already have one, then navigate to the "API Keys" section in your dashboard. Click on "New API Key", give it a name like "RAGtime", and copy the generated API key.

3. Build the Docker containers:

Navigate to the root directory of the cloned repository and execute the build script:

```bash
./build.sh
```

This script reads the `docker-compose.yml` file and builds the necessary Docker images for the Qdrant server and the Streamlit application.

### Running the Application

After successfully building the Docker containers, you can start the application by executing the start script:

```bash
./start.sh
```

This script starts the Docker containers defined in the `docker-compose.yml` file. Once the containers are up and running, you should be able to access the Streamlit application by navigating to `http://localhost:8501` in your web browser.

## Technical Overview

### Document Storage and Retrieval with Qdrant

Qdrant serves as the backbone for storing and retrieving document embeddings. Each document is transformed into a high-dimensional vector representation using advanced NLP models. These embeddings capture the semantic essence of the documents, allowing for precise and efficient similarity searches.

- **Document Embedding**: Utilizing either HuggingFace API or OpenAI API, embeddings are generated for each document. These embeddings are then stored in Qdrant, ready for efficient retrieval.

- **Efficient Retrieval**: When a user poses a question, its embedding is computed and used to query Qdrant for the most semantically similar document embeddings.


### Building the RAG Pipeline with Haystack

Haystack orchestrates the entire process, from document embedding to answer generation, through a carefully constructed pipeline known as the Retrieval-Augmented Generation (RAG) pipeline.

- **RAG Pipeline Components**: The pipeline consists of several interconnected components, including:
  - Text embedder for converting text to embeddings
  - Retriever for fetching relevant documents
  - Prompt builder for formatting prompts
  - Generator for producing candidate answers
  - Answer builder for constructing the final response

- **Dynamic Configuration**: The pipeline is dynamically configurable, allowing for the selection of different embedding and generator models based on the requirements of the task at hand.


## Codebase explained

### qdrant_document_store.py

This module defines a `DocumentStore` class that encapsulates the functionality needed to interact with a Qdrant document store. It handles document cleaning, splitting, embedding, and storage.

#### Class: DocumentStore

- **Initialization (`__init__`)**: Sets up the Qdrant document store with specific configurations such as URL, index name, embedding dimension, and optional HNSW parameters. It also initializes various preprocessing and writing components necessary for preparing and storing documents in the Qdrant store.

- **Components**:
    - `QdrantDocumentStore`: Configured with connection details and indexing options.
    - `DocumentCleaner`, `DocumentSplitter`: Used for cleaning and splitting documents before storage.
    - `HuggingFaceAPIDocumentEmbedder`: Generates embeddings for documents using a Hugging Face model.
    - `DocumentWriter`: Writes documents to the Qdrant store, configured to overwrite existing documents with the same ID.
    - `FileTypeRouter`, `TextFileToDocument`, `PyPDFToDocument`, `DocumentJoiner`: Handle file type routing and conversion of text files and PDFs to document objects.
    - `Pipeline`: Orchestrates the flow of data through the above components, connecting them in a logical sequence to process and store documents.

- **Method: run(docs)**: Executes the pipeline with the provided documents, processing them according to the setup components and storing the results in the Qdrant document store.

### ragtime_app.py

Sets up a Streamlit application that allows users to upload documents, which are then processed and stored using the `DocumentStore`. Users can interact with the stored documents through a chat interface powered by a Retrieval-Augmented Generation (RAG) model.

Key features:
- User authentication
- Document upload (PDF and TXT formats)
- Chat interface for asking questions
- Configuration options for embedding and generator models
- Multi-language support for chat interactions

#### Key Features:

- **Authentication**: Implements user authentication using a custom authenticator and configuration loaded from a YAML file.
- **Document Upload**: Provides a UI for uploading documents in PDF and TXT formats, which are then processed and stored.
- **Chat Interface**: Allows users to ask questions, which are answered by querying the stored documents using a RAG model.
- **Configuration**: Uses constants and configurations defined externally to customize behavior, such as selecting embedding and generator models.

### rag_retriever.py

Defines the `RAGRetriever` class, which constructs a RAG pipeline for retrieving relevant documents and generating answers to user queries.

#### Class: RAGRetriever

- **Initialization (`__init__`)**: Initializes the document store and sets up the embedding and generator models to be used in the RAG pipeline.
- **Method: build_rag_pipeline(initial_prompt, top_k)**: Constructs the RAG pipeline by adding and connecting various components, including text embedder, retriever, prompt builder, generator, and answer builder. These components work together to process user queries, retrieve relevant documents, generate responses, and format the final answer.

### constants.py

Contains constant values used across the application, including:
- Paths
- Model names
- Configuration settings for document splitting and retrieval
- Language selections for chat interface

### Docker Setup

The project includes Docker support for easy deployment and environment management.

- **docker-compose.yml**: Defines two services, `rag-qdrant` for the Qdrant server and `rag-streamlit` for the Streamlit application. It specifies the images to use, port mappings, volume mounts, and environment variables.
- **build.sh**: Script to build the Docker containers using the `docker-compose.yml` file.
- **start.sh**: Script to start the Docker containers defined in `docker-compose.yml`.

## Contact

For any inquiries or feedback, please open an issue on the Gitlab repository or contact me directly [here](mailto:tommaso.radicioni@aiknowyou.ai).

---

Feel free to adjust the content as necessary to match the specifics of your project, such as the generator/embedding models or additional features.
