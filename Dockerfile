# This sets up the container with Python 3.10 installed.
FROM python:3.10

# This sets the /code directory as the working directory for any RUN, CMD, ENTRYPOINT, or COPY instructions that follow.
WORKDIR /code

# Do not write .pyc files and disable output buffering to stdout/stderr
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# This runs pip install for all the packages listed in the requirements.txt file.
ADD requirements.txt .
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt

# This copies everything in the src directory to the /src directory in the container.
COPY src ./src