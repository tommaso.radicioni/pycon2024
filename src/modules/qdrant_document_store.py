import os

# Constants and functions defined in the codebase
import utils.constants as constants
from utils.funcs import get_document_embedder

# Haystack libraries
from haystack import Pipeline
from haystack.components.preprocessors import DocumentCleaner, DocumentSplitter
from haystack.components.writers import DocumentWriter
from haystack.components.converters import PyPDFToDocument, TextFileToDocument
from haystack.components.routers import FileTypeRouter
from haystack.components.joiners import DocumentJoiner

from haystack.document_stores.types import DuplicatePolicy

from haystack_integrations.document_stores.qdrant import QdrantDocumentStore


class DocumentStore:

    def __init__(self, index, embedding_service):

        # Define a Qdrant Document Store
        self.document_store = QdrantDocumentStore(
            url="http://rag-qdrant:6333/",
            index=index,
            embedding_dim=constants.EMBEDDING_SERVICE_TO_DIM[embedding_service],
            recreate_index=True,
            hnsw_config={"m": 16, "ef_construct": 64}  # Optional
        )

        # Define a Preprocessor in Haystack for document pre-processing before storage in Qdrant
        self.document_cleaner = DocumentCleaner(
        remove_empty_lines=True,
        remove_extra_whitespaces=True,
        remove_repeated_substrings=True,
        )

        # Define a Document Splitter with configurations
        self.document_splitter = DocumentSplitter(
            split_by=constants.SPLITTER_SPLIT_BY,
            split_length=constants.SPLITTER_LENGTH,
            split_overlap=constants.SPLITTER_OVERLAP
        )

        # Define a Document Embedder from Hugging Face API
        self.write_document_embedder = get_document_embedder(embedding_service)

        # Define a Document Writer with OVERWRITE policy to avoid duplicates
        self.document_writer = DocumentWriter(
            self.document_store, policy=DuplicatePolicy.OVERWRITE
        )

        # Define file type router and converters for text files and PDFs
        self.file_type_router = FileTypeRouter(mime_types=["text/plain", "application/pdf"])
        self.text_file_converter = TextFileToDocument()
        self.pdf_converter = PyPDFToDocument()
        self.document_joiner = DocumentJoiner()

        # Define a WritePipeline to write docs within Qdrant
        self.write_pipeline = Pipeline()
        self.write_pipeline.add_component(instance=self.file_type_router, name="file_type_router")
        self.write_pipeline.add_component(instance=self.text_file_converter, name="text_file_converter")
        self.write_pipeline.add_component(instance=self.pdf_converter, name="pypdf_converter")
        self.write_pipeline.add_component(instance=self.document_joiner, name="document_joiner")

        self.write_pipeline.add_component(instance=self.document_cleaner, name="document_cleaner")
        self.write_pipeline.add_component(instance=self.document_splitter, name="document_splitter")
        self.write_pipeline.add_component(instance=self.write_document_embedder, name="document_embedder")
        self.write_pipeline.add_component(instance=self.document_writer, name="document_writer")

        # Connect pipeline components for text files and PDFs through the pipeline
        self.write_pipeline.connect("file_type_router.text/plain", "text_file_converter.sources")
        self.write_pipeline.connect("file_type_router.application/pdf", "pypdf_converter.sources")
        self.write_pipeline.connect("text_file_converter", "document_joiner")
        self.write_pipeline.connect("pypdf_converter", "document_joiner")
        self.write_pipeline.connect("document_joiner", "document_cleaner")
        self.write_pipeline.connect("document_cleaner", "document_splitter")
        self.write_pipeline.connect("document_splitter", "document_embedder")
        self.write_pipeline.connect("document_embedder", "document_writer")
            
    def run(self, docs):
        result = self.write_pipeline.run({"file_type_router": {"sources": [docs]}})
        return result
