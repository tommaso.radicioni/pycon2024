import os

# Constants and functions defined in the codebase
import utils.constants as constants
from utils.funcs import get_text_embedder, get_llm_generator

# Haystack libraries
from haystack import Pipeline

from haystack_integrations.document_stores.qdrant import QdrantDocumentStore
from haystack_integrations.components.retrievers.qdrant import QdrantEmbeddingRetriever

from haystack.components.builders import PromptBuilder
from haystack.components.builders.answer_builder import AnswerBuilder

class RAGRetriever:

    """
    RAGRetriever is a class designed for building a Retrieval-Augmented Generation (RAG) pipeline.
    It initializes a document store using Qdrant integration with predefined configurations and builds
    a RAG pipeline by incorporating various components, namely the text embedder, retriever, prompt builder,
    generator, and answer builder.

    Attributes:
    index (str): The index name for the document store.
    embedding_model (str): The model name for the embedding component (HuggingFace API).
    generator_model (str): The model name for the generator component (HuggingFace API).

    Methods:
    build_rag_pipeline(initial_prompt: str, top_k: int): Constructs and returns the RAG pipeline with
    the specified initial prompt and top-k value for retrieving documents.
    """

    def __init__(self, index, embedding_service, generator_service):

        self.embedding_service = embedding_service
        self.generator_service = generator_service

        # Define a Document Store in Haystack. Service provided by ElasticSearch
        self.document_store = QdrantDocumentStore(
            url="http://rag-qdrant:6333/",
            index=index,
            embedding_dim=constants.EMBEDDING_SERVICE_TO_DIM[embedding_service],
            recreate_index=False,
            hnsw_config={"m": 16, "ef_construct": 64}  # Optional
        )

    # Build the RAG pipeline
    def build_rag_pipeline(self, initial_prompt, top_k):

        # Initialize the pipeline
        rag_pipeline = Pipeline()

        # Add a text embedder component using HuggingFace API
        rag_pipeline.add_component("embedder", 
                                   get_text_embedder(self.embedding_service)
        )

        # Add a retriever component using Qdrant embedding-based retriever
        rag_pipeline.add_component("retriever", QdrantEmbeddingRetriever(
            document_store=self.document_store,
            top_k=top_k
            )
        )

        # Add a prompt builder component to format the prompt
        rag_pipeline.add_component("prompt_builder", PromptBuilder(
            template=initial_prompt
            )
        )

        # Add a generator component using HuggingFace API
        rag_pipeline.add_component("llm", 
                                   get_llm_generator(self.generator_service)
                                   )
        
        # Add an answer builder component to construct the final answer
        rag_pipeline.add_component("answer_builder", AnswerBuilder())

        # Connect embedder's output to retriever's input
        rag_pipeline.connect("embedder.embedding", "retriever.query_embedding")
        # Connect retriever's output to prompt builder's input
        rag_pipeline.connect("retriever", "prompt_builder.documents")
        # Connect LLM's (language model) replies to answer builder's input
        rag_pipeline.connect("llm.replies", "answer_builder.replies")
        # Connect prompt builder's output to LLM's input
        rag_pipeline.connect("prompt_builder", "llm")
        # Connect LLM's metadata to answer builder's metadata
        rag_pipeline.connect("llm.meta", "answer_builder.meta")
        # Connect retriever's output to answer builder's input
        rag_pipeline.connect("retriever", "answer_builder.documents")

        return rag_pipeline