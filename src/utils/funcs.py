import os

from haystack.utils import Secret

from haystack.components.embedders import HuggingFaceAPIDocumentEmbedder, HuggingFaceAPITextEmbedder
from haystack.components.embedders import OpenAIDocumentEmbedder, OpenAITextEmbedder

from haystack.components.generators import HuggingFaceAPIGenerator
from haystack.components.generators import OpenAIGenerator

def get_document_embedder(generator_service):

    if generator_service == "HuggingFace":
        # Create a document embedder using HuggingFace API
        return HuggingFaceAPIDocumentEmbedder(
            api_type="serverless_inference_api",
            api_params={"model": "BAAI/bge-small-en-v1.5"},
            token=Secret.from_token(os.environ["HUGGINGFACE_API_KEY"])
        )

    elif generator_service == "OpenAI":
        # Create a document embedder using OpenAI API
        return OpenAIDocumentEmbedder(
            api_key=Secret.from_token(os.environ["OPENAI_API_KEY"]),
            model="text-embedding-3-large",
            dimensions=1536
        )

    else:
        # Raise exception if generator_service is unsupported
        raise Exception(f"Unsupported generator service: {generator_service}")

def get_text_embedder(generator_service):

    if generator_service == "HuggingFace":
        # Create a text embedder using HuggingFace API
        return HuggingFaceAPITextEmbedder(
                api_type="serverless_inference_api",
                api_params={"model": "BAAI/bge-small-en-v1.5"},
                token=Secret.from_token(os.environ["HUGGINGFACE_API_KEY"])
        )

    elif generator_service == "OpenAI":
        # Create a text embedder using OpenAI API
        return OpenAITextEmbedder(
            api_key=Secret.from_token(os.environ["OPENAI_API_KEY"]),
            model="text-embedding-3-large",
            dimensions=1536
        )
        
    else:
        # Raise exception if generator_service is unsupported
        raise Exception(f"Unsupported generator service: {generator_service}")
        

def get_llm_generator(generator_service):

    if generator_service == "HuggingFace":
        # Create a generator using HuggingFace API
        return HuggingFaceAPIGenerator(
            api_type="serverless_inference_api",
            api_params={"model": "HuggingFaceH4/zephyr-7b-beta"},
            token=Secret.from_token(os.environ["HUGGINGFACE_API_KEY"])
        )

    elif generator_service == "Groq":
        # Create a generator using Groq API
        return OpenAIGenerator(
            api_key=Secret.from_token(os.environ["GROQ_API_KEY"]),
            api_base_url="https://api.groq.com/openai/v1",
            model="llama-3.1-70b-versatile",
            generation_kwargs={
                "temperature": 0.,
                "max_tokens": 4096,
            },
        )

    else:
        # Raise exception if generator_service is unsupported
        raise Exception(f"Unsupported generator service: {generator_service}")

