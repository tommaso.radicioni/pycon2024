# LOCAL DIRECTORY FOR TEMP FILES
LOCAL_DOCS_DIR = 'src/local_docs'

# QDRANT
SPLITTER_SPLIT_BY = "word"
SPLITTER_LENGTH = 150
SPLITTER_OVERLAP = 50

# RAG RETRIEVER
TOP_K = 5

# MODELS

EMBEDDING_SERVICE_SELECTION = ["OpenAI", "HuggingFace"] 
GENERATOR_SERVICE_SELECTION = ["Groq", "HuggingFace"]

EMBEDDING_SERVICE_TO_DIM = {"OpenAI": 1536, "HuggingFace": 384}
GENERATOR_LANGUAGE_SELECTIONS = ["Italian","English","French","Spanish","German"]

# PROMPTS
LANG_PLACEHOLDER = "<LANGUAGE>"
INITIAL_PROMPT = """
Given the following information, answer the question ALWAYS in <LANGUAGE>. 

Context:
{% for document in documents %}
    {{ document.content }}
{% endfor %}

Question: {{question}}
Answer:
"""