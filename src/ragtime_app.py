import os 

import streamlit as st
import streamlit_authenticator as stauth
import yaml

from streamlit_chat import message

from streamlit_extras.colored_header import colored_header
from streamlit_extras.add_vertical_space import add_vertical_space

from modules.qdrant_document_store import DocumentStore
from modules.rag_retriever import RAGRetriever
from pathlib import Path

try:
    import src
except:
    import sys
    sys.path.insert(0, './')

import utils.constants as constants

ss = st.session_state

# Load authentication configuration from a YAML file
with open('src/config.yaml') as file:
    config = yaml.load(file, Loader=yaml.SafeLoader)

# Extract and store passwords from the config for authentication purposes
passwords = []
for key, value in config['credentials']['usernames'].items():
    passwords.append(value['password'])

# Initialize the Streamlit authenticator with configuration settings
authenticator = stauth.Authenticate(
    config['credentials'],
    config['cookie']['name'],
    config['cookie']['key'],
    config['cookie']['expiry_days'],
    config['preauthorized']
)
# Perform login authentication
authenticator.login()

# Error handling for login status
if ss["authentication_status"] is False:
    st.error('Username/password is incorrect')
elif ss["authentication_status"] is None:
    st.warning('Please enter your username and password')
elif ss["authentication_status"]:

    # Get the username from session state
    username = ss["username"]

    # Define the local directory path based on the username
    local_dir_path = os.path.join(constants.LOCAL_DOCS_DIR,username)

    # Create local directory if it doesn't exist
    if not os.path.exists(local_dir_path):
        os.mkdir(local_dir_path)

    with st.sidebar:

        # Provide a logout button in the sidebar
        authenticator.logout()

        # Title and description for the Streamlit app
        st.title('💬 RAGTime: A simple RAG Pipeline!')

        st.markdown('## How to use the demo? \n This demo is a conversational chatbot answering questions on your documentation.')
        st.markdown('Below you can find the steps for using the app: \n - Upload documents; \n - Start a conversation!')
        add_vertical_space(5)
        st.write('Made by [Tommaso Radicioni](https://aiknowyou.ai/)')
        add_vertical_space(2)

        # Select box for choosing the embedding model
        embedding_service = st.selectbox(
            "Embedding model:",
            constants.EMBEDDING_SERVICE_SELECTION)

        # Select box for choosing the generator model
        generator_service = st.selectbox(
            "Generator model:",
            constants.GENERATOR_SERVICE_SELECTION)
        
        # Select box for choosing the generator model
        generator_language = st.selectbox(
            "Chat Language:",
            constants.GENERATOR_LANGUAGE_SELECTIONS)

    # Initialize the RAG retriever with parameters
    rag_retriever = RAGRetriever(
        index = username, 
        embedding_service = embedding_service,
        generator_service = generator_service)
    
    
    generator_prompt = constants.INITIAL_PROMPT.replace(constants.LANG_PLACEHOLDER, generator_language)

    # Build the RAG search pipeline
    rag_pipeline = rag_retriever.build_rag_pipeline(
        initial_prompt = generator_prompt,
        top_k = constants.TOP_K
    )

    # Initialize the DocumentStore with parameters
    doc_store = DocumentStore(
        index = username,
        embedding_service = embedding_service)

    ################################
    # Section 1 - Upload Documents #
    ################################

    st.markdown("## 1 - Upload your documents")
    
    # File uploader for PDF and TXT files
    uploaded_file = st.file_uploader("Upload documents in PDF and TXT format", type=["pdf","txt"])

    if uploaded_file: 

        st.text("")

        # Confirmation to add document to the database
        st.write("Are you sure to add '{}' to the database?".format(uploaded_file.name))
        add_button = st.button("Add document")

        if add_button:

            db_update_spinner = st.spinner("Updating database....")

            # Save the uploaded file to the local directory
            with open(os.path.join(local_dir_path,uploaded_file.name), 'wb') as f:
                f.write(uploaded_file.getbuffer())

            # Temporary local file path
            temp_local_file = os.path.join(local_dir_path,uploaded_file.name)

            # Update the document store with the new document
            with db_update_spinner:
                result = doc_store.run(temp_local_file)
                st.text(result)
                st.text("Documents succesfully added!")

                os.remove(temp_local_file) 
    
    add_vertical_space(3)
    
    ######################################################
    # Section 2 - Start chatting with the uploaded files #
    ######################################################

    st.markdown('## 2 - Chat with your files')

    # Function to handle user input changes
    def on_input_change():
        user_input = ss.user_input
        ss.past.append(user_input)

        # Run the RAG pipeline to get a response
        output = rag_pipeline.run({
            "embedder": {"text": user_input},
            "prompt_builder": {"question": user_input},
            "answer_builder": {"query": user_input}
        })

        response_content = str(output["answer_builder"]["answers"][0].data)
        docs_retrieved = list(output["answer_builder"]["answers"][0].documents)
        unique_docs = str(list(set([doc.meta["file_path"].split("/")[-1] for doc in docs_retrieved])))


        # Append the generated response to session state
        final_answer = response_content + "\n\n Retrieved Documents:" + unique_docs
        ss.generated.append(final_answer)

    # Function to clear the chat history
    def on_btn_click():
        del ss.past[:]
        del ss.generated[:]

    # Initialize past and generated chat history in session state
    ss.setdefault('past', [])
    ss.setdefault('generated', [])

    # Placeholder for the chat interface
    chat_placeholder = st.empty()

    with chat_placeholder.container():    
        for i in range(len(ss['generated'])):              
            message(ss['past'][i], is_user=True, key=f"{i}_user", avatar_style="micah", seed=14)
            message(
                ss['generated'][i], 
                key=f"{i}", 
                allow_html=True,
                is_table=False
            )

        # Button to clear chat messages
        st.button("Clear message", on_click=on_btn_click)

    # Text input for user to type their messages
    with st.container():
        st.text_input("User Input:", on_change=on_input_change, key="user_input")




