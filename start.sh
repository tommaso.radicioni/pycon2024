#!/bin/bash

# Jump to "self" folder
cd "$(dirname "$0")"

# Containers up
docker compose --env-file .env -f ./docker-compose.yml up -d --build