haystack-ai==2.1.2
huggingface-hub==0.23.1
pypdf==4.2.0
qdrant-haystack==3.5.0
streamlit==1.34.0
streamlit-authenticator==0.3.2
streamlit-chat==0.1.1
streamlit-extras==0.4.2
